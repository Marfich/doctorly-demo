variable "aws_region" {
  description = "The AWS region "
  default     = "ap-southeast-2"
}

variable "ami" {
  description = "AMI id"
  default     = "xyz"
}

variable "key_name" {
  description = "Name of the solution"
  default     = "test"
}