provider "aws" {
  region     = "${var.aws_region}"
  access_key = "****"
  secret_key = "****"
}
resource "aws_instance" "example" {
  ami           = "${var.ami}"
  instance_type = "t2.micro"
  key_name      = "${var.key_name}"

  tags = {
    Name = "${var.key_name}"
  }

  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = "${file("test.pem")}"
    host        = "${self.public_ip}"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo amazon-linux-extras install ansible2 -y",
      "sudo yum install git -y",
      "git clone https://gitlab.com/marfich /tmp/ansible",
      "cd /tmp/ansible",
      "ansible-playbook apply-role.yaml"
    ]
  }
}