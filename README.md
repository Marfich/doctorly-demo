# Demo task solution for doctorly

by Marfenko Sergey

marfenkos@gmail.com

## Task 1

- Task was implemented based on docker-docs [example](https://github.com/docker/awesome-compose/tree/master/nginx-aspnet-mysql) with .net sample app and mariadb/mysql
- Ansible playbook/role was tested for the CentOS 8 env: no "flex" options for checking the OS-family
- Tests were not so successful: my x86_64 VM has no access to docker hub sadly, and my localhost is MacOS with arm64 :(
  that's why I was enable to try everything together.


#### Run guide:

```commandline
$ cd task1/ansible 
$ vim hosts  # adjust hosts in inventory
$ ansible-playbook apply-role.yml  # we hope that SSH credentials were already setup!
```

## Task 2
The idea is quite simple: separate files/directories for various cloud providers and single file with variables:
some of them are env-specific, some are common.
I have created a scaffolding for AWS, but didn't have enough time for GCP

## Task 3

- I would use some traditional monitoring solution for this standard problem:
for example the Prometheus/Grafana stack
- We'll need the central server with:
  - Prometheus server (metrics scrapper and aggregator)
  - Grafana with user-friendly web-dashboards
  - Alert Manager for setting up alert/notification system: threshold reached -> notify admins into Slack
    (actually the internal Grafana notifications can also be used)
- Every host "under monitoring" should have exporters 
  (applications which expose some metrics via HTTP protocol to server):
  - node_exporter: basic system metrics CPU/memory/swap...
  - application specific exporters: we don't need just Ubuntu, we are using some applications 
  and there's a chance that the specific exporter for this application is available
- Grafana dashboards can be setup based on typical needs
- In terms of High-Availability we can setup the additional Prometheus server
  and consider using a redundant Grafana and Alert Manager configuration